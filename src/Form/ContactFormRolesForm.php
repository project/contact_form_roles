<?php
/**
 * @file
 * Contains \Drupal\contact_form_roles\Form\ContactFormRolesForm.
 */

namespace Drupal\contact_form_roles\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\contact\ContactFormInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;

/**
 * Defines a form to configure maintenance settings for this site.
 */
class ContactFormRolesForm extends ConfigFormBase {

  protected $contactForm;

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'config_form_roles_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['contact_form_roles.forms'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('contact_form_roles.forms');
    $buildInfo = $form_state->getBuildInfo();
    $formId = $buildInfo['args'][0]['contactFormEntity']->id();

    $config_value = $config->get('form.' . $formId);
    if (empty($config_value)) {
      $config_value = [];
    }

    $possible_roles = array_map(function($r) {
      return $r->label();
    }, user_roles());

    $form['roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles that can access this form'),
      '#options' => $possible_roles,
      '#default_value' => $config_value,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('contact_form_roles.forms');
    $buildInfo = $form_state->getBuildInfo();
    $formId = $buildInfo['args'][0]['contactFormEntity']->id();

    $configValue = [];
    $options = $form_state->getValue(['roles']);
    $options = array_filter($options);
    $config->set('form.' . $formId, array_keys($options));

    $config->save();
    Cache::invalidateTags(['config:core.entity_form_display.contact_message.' . $formId . '.default']);
    parent::submitForm($form, $form_state);
  }
}
