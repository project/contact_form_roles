<?php

/**
 * @file
 * Contains \Drupal\contact_form_roles\Controller\ContactFormRolesController.
 */

namespace Drupal\contact_form_roles\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\contact\ContactFormInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller routines for contact routes.
 */
class ContactFormRolesController extends ControllerBase {
  /**
   * Form constructor for the personal contact form role editor.
   *
   * @param \Drupal\contact\ContactFormInterface $contactForm
   *   The contact form to be modified.
   *
   * @return array
   *   The personal contact form as render array as expected by drupal_render().
   */
  public function rolesForm($contact_form = NULL) {
    $contactFormEntity = NULL;
    $contactFormName = NULL;

    if (empty($contact_form)) {
      // Use the default form if no form has been passed.
      $contactFormName = $this->config('contact.settings')
        ->get('default_form');
    }
    else {
      // Load the contact form by name.
      $contactFormName = $contact_form;
    }
    $contactFormEntity = \Drupal::service('entity_type.manager')
      ->getStorage('contact_form')
      ->load($contactFormName);

    // If there are no forms, do not display the form.
    if (empty($contactFormEntity)) {
      if ($this->currentUser()->hasPermission('administer contact forms')) {
        $this->messenger()->addError($this->t('The contact form has not been configured. <a href=":add">Add one or more forms</a> .', array(
          ':add' => $this->url('contact.form_add'))));
        return array();
      }
      else {
        throw new NotFoundHttpException();
      }
    }

    $form = \Drupal::formBuilder()->getForm(
      'Drupal\contact_form_roles\Form\ContactFormRolesForm',
      ['contactFormEntity' => $contactFormEntity]
    );
    return $form;
  }
}
